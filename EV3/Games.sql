-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: db
-- Tiempo de generación: 03-08-2021 a las 02:36:12
-- Versión del servidor: 8.0.25
-- Versión de PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Games`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `juegos`
--

CREATE TABLE `juegos` (
  `idJuegos` int NOT NULL,
  `nombreJuego` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `stock` int NOT NULL,
  `categoria` varchar(30) NOT NULL,
  `company` varchar(30) NOT NULL,
  `fechaLan` date NOT NULL,
  `precio` int NOT NULL,
  `fotoMain` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `fotoLogo` varchar(500) NOT NULL,
  `multiplayer` tinyint(1) NOT NULL,
  `promo` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `juegos`
--

INSERT INTO `juegos` (`idJuegos`, `nombreJuego`, `stock`, `categoria`, `company`, `fechaLan`, `precio`, `fotoMain`, `fotoLogo`, `multiplayer`, `promo`) VALUES
(1, 'Resident Evil 3', 20, 'Acción', 'Capcom', '2020-04-02', 39900, 'https://cdn.cloudflare.steamstatic.com/steam/apps/952060/ss_77eda710487b89293f109cf7dcf96b4ffab0d1a1.1920x1080.jpg?t=1614737794', 'https://cdn.cloudflare.steamstatic.com/steam/apps/952060/header.jpg?t=1614737794', 1, 0),
(2, 'F1® 2021', 300, 'Deportes', 'Codemasters', '2021-07-15', 43900, 'https://cdn.cloudflare.steamstatic.com/steam/apps/1134570/ss_504e0ec2205e938fe459e02c43021c28b12f0fbf.1920x1080.jpg?t=1626784622', 'https://cdn.cloudflare.steamstatic.com/steam/apps/1134570/header.jpg?t=1626784622', 1, 0),
(3, 'Sid Meiers Civilization® VI', 1000, 'Estrategia', 'Firaxis Games', '2016-10-21', 44990, 'https://cdn.cloudflare.steamstatic.com/steam/apps/289070/ss_a4b07a0fbdd09e35b5ec3a4726239b884f1f1f7d.1920x1080.jpg?t=1626210772', 'https://cdn.cloudflare.steamstatic.com/steam/apps/289070/header.jpg?t=1626210772', 1, 1),
(4, 'DEATH STRANDING', 155, 'Acción', 'KOJIMA PRODUCTIONS', '2020-07-14', 41990, 'https://cdn.cloudflare.steamstatic.com/steam/apps/1190460/ss_ac7c64c8d10bb5786694891e4a22b07a5da7dd6f.1920x1080.jpg?t=1616683107', 'https://cdn.cloudflare.steamstatic.com/steam/apps/1190460/header.jpg?t=1616683107', 0, 0),
(5, 'Halo: The Master Chief Collection', 100, 'Acción', 'Xbox Game Studios', '2019-12-03', 19990, 'https://cdn.cloudflare.steamstatic.com/steam/apps/976730/ss_62bbd86f4735893ef6cd53206cf8c93f87eb86ec.1920x1080.jpg?t=1624467656', 'https://cdn.cloudflare.steamstatic.com/steam/apps/976730/header.jpg?t=1624467656', 1, 0),
(6, 'Super Robot Wars 30', 555, 'Estrategia', 'BANDAI NAMCO Entertainment', '2021-10-27', 28500, 'urlhttps://cdn.cloudflare.steamstatic.com/steam/apps/898750/ss_30df065a2f94cf422ec0da83195c5e7578c747c1.1920x1080.jpg?t=1626856069', 'https://cdn.cloudflare.steamstatic.com/steam/apps/898750/header.jpg?t=1626856069', 0, 0),
(7, 'FINAL FANTASY XIV Online', 999, 'RPG', 'Square Enix', '2014-02-18', 11450, 'https://cdn.cloudflare.steamstatic.com/steam/apps/39210/ss_d37db153382217ad479679691899588e8d173937.1920x1080.jpg?t=1621453463', 'https://cdn.cloudflare.steamstatic.com/steam/apps/39210/header.jpg?t=1621453463', 1, 0),
(8, 'FIFA 21', 555, 'Deportes', 'Electronic Arts', '2020-10-09', 47900, 'https://cdn.cloudflare.steamstatic.com/steam/apps/1313860/ss_d69a3f9e19bbb90f09e4eef86c5f686c7edbb82e.1920x1080.jpg?t=1625577202', 'https://cdn.cloudflare.steamstatic.com/steam/apps/1313860/header.jpg?t=1625577202', 1, 1),
(9, 'Monster Hunter Stories 2: Wings of Ruin', 2323, 'RPG', 'Capcom', '2021-07-09', 39900, 'https://cdn.cloudflare.steamstatic.com/steam/apps/1277400/ss_655f8bf7931590653277444466934084294e6c44.1920x1080.jpg?t=1626120014', 'https://cdn.cloudflare.steamstatic.com/steam/apps/1277400/header.jpg?t=1626120014', 1, 0),
(10, 'NBA 2K21', 4333, 'Deportes', '2K', '2020-09-04', 49990, 'https://cdn.cloudflare.steamstatic.com/steam/apps/1225330/ss_19466ddcb738a6fe52a9da3d849fac40daa6d885.1920x1080.jpg?t=1614622937', 'https://cdn.cloudflare.steamstatic.com/steam/apps/1225330/header.jpg?t=1614622937', 1, 1),
(11, 'Age of Empires IV', 1000, 'Estrategia', 'Xbox Game Studios', '2021-10-28', 29990, 'https://cdn.cloudflare.steamstatic.com/steam/apps/1466860/ss_48195285a60c6208f8bd722f74c556b9a224f4b0.1920x1080.jpg?t=1623794834', 'https://cdn.cloudflare.steamstatic.com/steam/apps/1466860/header.jpg?t=1623794834', 1, 0),
(12, 'Yakuza: Like a Dragon', 521, 'RPG', 'SEGA', '2020-11-10', 29999, 'https://cdn.cloudflare.steamstatic.com/steam/apps/1235140/ss_d8bcb8c72368ec09506d3a60d42ff2a1901e39f7.1920x1080.jpg?t=1627295192', 'https://cdn.cloudflare.steamstatic.com/steam/apps/1235140/header.jpg?t=1627295192', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `idUser` int NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `rut` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`idUser`, `nombre`, `password`, `rut`, `email`) VALUES
(1, 'Daniel Avila', '123456', '15173274-7', 'admin@mail.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `juegos`
--
ALTER TABLE `juegos`
  ADD PRIMARY KEY (`idJuegos`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `juegos`
--
ALTER TABLE `juegos`
  MODIFY `idJuegos` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
