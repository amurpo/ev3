package cl.Inacap.Controllers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Juego;
import Servicios.JuegosService;
import Servicios.JuegosServiceLocal;

/**
 * Servlet implementation class RegistroJuego
 */
@WebServlet("/RegistroJuego.do")
public class RegistroJuego extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistroJuego() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Inject
    private JuegosServiceLocal juegoService;
    
    public static Date parseDate(String date) {
    	try {
    		return new SimpleDateFormat("yyyy-MM-dd").parse(date);
    	} catch (Exception e) {
    		return null;
    	}
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		
		request.getRequestDispatcher("Site/registroJuegos.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Date fecha = parseDate("1990-09-05");
		
		String nombreJuego = request.getParameter("NombreJuego").toString();
		int stock = Integer.parseInt(request.getParameter("Stock").toString());
		String categoria = request.getParameter("Categoria").toString();
		String company = request.getParameter("Company").toString();
		//Date fecha = request.getParameter("FechaLan").toString();
		int precio = Integer.parseInt(request.getParameter("Precio").toString());
		String fotoMain = request.getParameter("FotoMain").toString();
		String fotoLogo = request.getParameter("FotoLogo").toString();

		/*
		 * boolean booleanMultiplayer = true; if (booleanMultiplayer.equals("Si")) {
		 * booleanMultiplayer = true; } else if (booleanMultiplayer.equals("No")) {
		 * booleanMultiplayer = false; }
		 */
		
		
		Juego u=new Juego();
		u.setNombreJuego(nombreJuego);
		u.setStock(stock);
		u.setCategoria(categoria);
		u.setCompany(company);
		u.setFechaLan(fecha);
		u.setPrecio(precio);
		u.setFotoMain(fotoMain);
		u.setFotoLogo(fotoLogo);
		u.setMultiplayer(false);
		
		juegoService.addJuego(u);		doGet(request, response);
		response.sendRedirect("Home.do");
	}

}
