package cl.Inacap.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Juego;
import Servicios.JuegosServiceLocal;

/**
 * Servlet implementation class Deporte
 */
@WebServlet("/Deporte.do")
public class Deporte extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Deporte() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Inject
    private JuegosServiceLocal juegoService;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out= response.getWriter();
		List<Juego> listJuegosDeportes=new ArrayList<Juego>(); 
		
		
		listJuegosDeportes=juegoService.getAllJuegos()	.stream()
												.filter(p->p.getCategoria().matches("Deportes"))
												.collect(Collectors.toList());
			

		request.setAttribute("listJuegosDeportes", listJuegosDeportes);		
		
		request.getRequestDispatcher("Site/deportes.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
