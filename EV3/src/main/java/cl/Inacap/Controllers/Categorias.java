package cl.Inacap.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Servicios.JuegosServiceLocal;
import Model.Juego;
/**
 * Servlet implementation class Categorias
 */
@WebServlet("/Categorias.do")
public class Categorias extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Categorias() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Inject
    private JuegosServiceLocal juegoService;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out= response.getWriter();
		List<Juego> listJuegosAccion=new ArrayList<Juego>(); 
		
		
		listJuegosAccion=juegoService.getAllJuegos()	.stream()
												.filter(p->p.getCategoria().matches("Acción"))
												.collect(Collectors.toList());
		
		List<Juego> listJuegosDeportes=new ArrayList<Juego>(); 
		listJuegosDeportes=juegoService.getAllJuegos()	.stream()
				.filter(p->p.getCategoria().matches("Deportes"))
				.collect(Collectors.toList());
		
		List<Juego> listJuegosEstrategia=new ArrayList<Juego>(); 
		listJuegosEstrategia=juegoService.getAllJuegos()	.stream()
				.filter(p->p.getCategoria().matches("Estrategia"))
				.collect(Collectors.toList());
		
		List<Juego> listJuegosRPG=new ArrayList<Juego>(); 
		listJuegosRPG=juegoService.getAllJuegos()	.stream()
				.filter(p->p.getCategoria().matches("RPG"))
				.collect(Collectors.toList());
		
		
		
		request.setAttribute("listJuegosRPG", listJuegosRPG);		
		
		request.setAttribute("listJuegosEstrategia", listJuegosEstrategia);		
		
		request.setAttribute("listJuegosDeportes", listJuegosDeportes);		

		request.setAttribute("listJuegosAccion", listJuegosAccion);		
		
		request.getRequestDispatcher("Site/categorias.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
