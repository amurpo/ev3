package cl.Inacap.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Servicios.JuegosServiceLocal;
import Model.Juego;

/**
 * Servlet implementation class Ofertas
 */
@WebServlet("/Ofertas.do")
public class Ofertas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Ofertas() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Inject
    private JuegosServiceLocal juegoService;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out= response.getWriter();
		List<Juego> listJuegosOferta=new ArrayList<Juego>(); 
		
		
		listJuegosOferta=juegoService.getAllJuegos()	.stream()
												.filter(p->p.isPromo()==true)
												
												.collect(Collectors.toList());	
		//.forEach(p -> p.setUserCanEdit(true));

		request.setAttribute("listJuegosOferta", listJuegosOferta);		
		
		request.getRequestDispatcher("Site/ofertas.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
