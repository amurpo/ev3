package cl.Inacap.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Juego;
import Servicios.JuegosServiceLocal;


/**
 * Servlet implementation class Accion
 */
@WebServlet("/Accion.do")
public class Accion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Accion() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Inject
    private JuegosServiceLocal juegoService;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out= response.getWriter();
		List<Juego> listJuegosAccion=new ArrayList<Juego>(); 
		
		
		listJuegosAccion=juegoService.getAllJuegos()	.stream()
												.filter(p->p.getCategoria().matches("Acción"))
												.collect(Collectors.toList());
			

		request.setAttribute("listJuegosAccion", listJuegosAccion);		
		
		request.getRequestDispatcher("Site/accion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
