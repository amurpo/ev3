<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Payara Games | Compra</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
      <!-- Custom styles -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/Site/css/style.min.css" />
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
      <link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick-theme.css">
      <style>
         .col-centered{
         float: none;
         margin: 0 auto;
         }
         
      </style>
   </head>
   <body>
      <div class="layer"></div>
      <!-- ! Body -->
      <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
      <div class="page-flex">
      
       <!-- ! Sidebar -->
   <jsp:include page="menu.jsp"/>
     
     
      <div class="main-wrapper">
      
         <!-- ! Main nav -->
   <jsp:include page="header.jsp"/>

         <br><br>
         <!-- ! Main -->
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-10 col-centered">
                  <h2 style="text-align: center;" class="main-title">Confirme su compra</h2>
                  <br>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="users-table table-wrapper">
                           <table class="posts-table">
                              <thead>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       <div class="categories-table-img">
                                          <picture>
                                             <img src="https://cdn.cloudflare.steamstatic.com/steam/apps/952060/header.jpg?t=1614737794" alt="category">
                                          </picture>
                                       </div>
                                    </td>
                                    <td>
                                       Resident Evil 3
                                    </td>
                                    <td>
                                       <div class="pages-table-img">
                                          Acción
                                       </div>
                                    </td>
                                    <td><span class="badge-success">En Oferta</span></td>
                                    <td>$ 13.167</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="row">
                     <div style="text-align: center;" class="col-md-3 col-centered">
                        <a class="primary-default-btn" href="/EV3" onclick="alert('Gracias por su compra!');" role="button">COMPRAR</a>
            
                     </div>
                  </div>
               </div>
            </div>
            </main>
            
         <!-- ! Footer -->
     <jsp:include page="footer.jsp"/>
        
        
         </div>
      </div>
      <!-- Chart library -->
      <script src="https://webapp.ioseg.com/img/plugins/chart.min.js"></script>
      <!-- Icons library -->
      <script src="https://webapp.ioseg.com/img/plugins/feather.min.js"></script>
      <!-- Custom scripts -->
      <script src="https://webapp.ioseg.com/js/script.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
   </body>
</html>