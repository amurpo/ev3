<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
   
<!DOCTYPE html>
<footer class="footer">
<div class="container footer--flex">
   <div class="footer-start">
      <p>2021 © Payara Games</p>
   </div>
   <ul class="footer-end">
      <li><a href="./Nosotros.do">Sobre nosotros</a></li>
      <li><a href="https://gitlab.com/daigo.tnt/EV3">Gitlab</a></li>
      <li><a href="https://dashboard.elegant-goodies.com/">Dashboard</a></li>
   </ul>
</div>
</footer>

