<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Payara Games | Nosotros</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
      <!-- Custom styles -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/Site/css/style.min.css" />
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
      <link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick-theme.css">
      <style>
         .team-member {
         text-align: center;
         }
      </style>
   </head>
   <body>
      <div class="layer"></div>
      <!-- ! Body -->
      <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
      <div class="page-flex">
      
              <!-- ! Sidebar -->
   <jsp:include page="menu.jsp"/>
   
          <div class="main-wrapper">
         <!-- ! Main nav -->
   <jsp:include page="header.jsp"/>
   
            <!-- ! Main -->
            <!-- Section - Our Team -->
            <br><br>
            <div class="section-content section-content--secondary">
               <div class="container">
                  <div class="section-heading section-heading--divider-top">
                     <h2 style="text-align: center;" class="main-title">Nuestro Team</h2>
                  </div>
                  <br>
                  <ul class="team team--grid list-unstyled row">
                     <li class="team-member col-sm">
                        <figure class="team-member__photo"><a href="mailto:nano@email.com"><img src="http://escapium.dan-fisher.com/assets/img/samples/member-photo-1.jpg" alt=""></a></figure>
                        <div class="team-member__info">
                           <h3 class="team-member__title">Nano Cervantes</h3>
                           <p class="team-member__meta">Co-Creador</p>
                        </div>
                     </li>
                     <li class="team-member col-sm">
                        <figure class="team-member__photo"><a href="mailto:pansha@email.com"><img src="http://escapium.dan-fisher.com/assets/img/samples/member-photo-2.jpg" alt=""></a></figure>
                        <div class="team-member__info">
                           <h3 class="team-member__title">Loca Pansha</h3>
                           <p class="team-member__meta">Co-Creador</p>
                        </div>
                     </li>
                     <li class="team-member col-sm">
                        <figure class="team-member__photo"><a href="mailto:silvia@email.com"><img src="http://escapium.dan-fisher.com/assets/img/samples/member-photo-3.jpg" alt=""></a></figure>
                        <div class="team-member__info">
                           <h3 class="team-member__title">Silvia Almendra</h3>
                           <p class="team-member__meta">Co-Creador</p>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </main>

            <!-- Section - Our Team / End -->
            <!-- ! Footer -->
            <footer class="footer">
               <div class="container footer--flex">
                  <div class="footer-start">
                     <p>2021 © Payara Games</p>
                  </div>
                  <ul class="footer-end">
                     <li><a href="./nosotros.html">Sobre nosotros</a></li>
                     <li><a href="https://gitlab.com/daigo.tnt/EV3">Gitlab</a></li>
                     <li><a href="https://dashboard.elegant-goodies.com/">Dashboard</a></li>
                  </ul>
               </div>
            </footer>
         </div>
      </div>
      <!-- Chart library -->
      <script src="https://webapp.ioseg.com/img/plugins/chart.min.js"></script>
      <!-- Icons library -->
      <script src="https://webapp.ioseg.com/img/plugins/feather.min.js"></script>
      <!-- Custom scripts -->
      <script src="https://webapp.ioseg.com/js/script.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
   
   </body>
</html>
