<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  


<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Payara Games | Categorias</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
      <!-- Custom styles -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/Site/css/style.min.css" />
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
      <link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick-theme.css">
      <style>
         .col-centered{
         float: none;
         margin: 0 auto;
         
         }
         [data-href] {
    cursor: pointer;
}
      </style>
   </head>
   <body>
      <div class="layer"></div>
      <!-- ! Body -->
      <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
      <div class="page-flex">
      
             <!-- ! Sidebar -->
   <jsp:include page="menu.jsp"/>
     
     
      <div class="main-wrapper">
         <!-- ! Main nav -->
   <jsp:include page="header.jsp"/>
   
            <!-- ! Main -->
            <main class="main users chart-page" id="skip-target">
               <div class="container">
               </div>
               <br>
               <div class="row">
                  <div class="col-lg-9 col-centered">
                     <h2 style="text-align: center;" class="main-title">Todas las categorías</h2>

                     <div class="users-table table-wrapper">
                        <table class="posts-table">
                           <thead>
                              <tr class="users-table-info">

                                 <th style="font-weight: bold;font-size:large;">Acción</th>

                              </tr>
                           </thead>
                           <tbody>
                           
                           <c:forEach items="${listJuegosAccion}"  var="j" varStatus="recorrido">
                             
                              <tr data-href="Juego.do">
                                 <td>
                                       <div class="categories-table-img">
                                          <picture>
                                             <img src=${j.getFotoLogo ()} alt="category">
                                          </picture>
                                       </div>
                                 </td>
                                 <td>
                                  ${j.getNombreJuego() }
                                 </td>
                                 <td>
                                    <div class="pages-table-img">
                                       ${j.getCategoria() }
                                    </div>
                                 </td>
                                 <td><span class="badge-success">${ j.isPromo() ? "En Oferta" : "" }</span></td>
                                 <td>$ ${j.getPrecio() }</td>
                             
                              </tr>
                           
                           </c:forEach>

                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
               <div class="row">
                <div class="col-lg-9 col-centered">
                   <div class="users-table table-wrapper">
                      <table class="posts-table">
                         <thead>
                            <tr class="users-table-info">

                               <th style="font-weight: bold;font-size:large;">Deportes</th>

                            </tr>
                         </thead>
                         <tbody>
                         
                         <c:forEach items="${listJuegosDeportes}"  var="j" varStatus="recorrido">
                         
                              <tr data-href="Juego.do">
                               <td>
                                     <div class="categories-table-img">
                                        <picture>
                                           <img src=${j.getFotoLogo ()} alt="category">
                                        </picture>
                                     </div>
                               </td>
                               <td>
                                ${j.getNombreJuego() }
                               </td>
                               <td>
                                  <div class="pages-table-img">
                                     ${j.getCategoria() }
                                  </div>
                               </td>
                                 <td><span class="badge-success">${ j.isPromo() ? "En Oferta" : "" }</span></td>
                               <td>$ ${j.getPrecio() }</td>
                           
                             
                                                          
                              </tr>
                          </c:forEach>
                                                            
                         </tbody>
                      </table>
                   </div>
                </div>
             </div>
             <div class="row">
              <div class="col-lg-9 col-centered">
                 <div class="users-table table-wrapper">
                  <table class="posts-table">
                     <thead>
                        <tr class="users-table-info">

                           <th style="font-weight: bold;font-size:large;">Estrategia</th>

                        </tr>
                     </thead>
                     <tbody>
                         
                         <c:forEach items="${listJuegosEstrategia}"  var="j" varStatus="recorrido">
                         
                              <tr data-href="Juego.do">
                               <td>
                                     <div class="categories-table-img">
                                        <picture>
                                           <img src=${j.getFotoLogo ()} alt="category">
                                        </picture>
                                     </div>
                               </td>
                               <td>
                                ${j.getNombreJuego() }
                               </td>
                               <td>
                                  <div class="pages-table-img">
                                     ${j.getCategoria() }
                                  </div>
                               </td>
                                 <td><span class="badge-success">${ j.isPromo() ? "En Oferta" : "" }</span></td>
                               <td>$ ${j.getPrecio() }</td>
                           
                             
                                                          
                              </tr>
                          </c:forEach>
                                                            
                         </tbody>
                  </table>
                 </div>
              </div>
           </div>
           <div class="row">
            <div class="col-lg-9 col-centered">
               <div class="users-table table-wrapper">
                  <table class="posts-table">
                     <thead>
                        <tr class="users-table-info">

                           <th style="font-weight: bold;font-size:large;">RPG</th>

                        </tr>
                     </thead>
                                             <tbody>
                         
                         <c:forEach items="${listJuegosRPG}"  var="j" varStatus="recorrido">
                         
                              <tr data-href="Juego.do">
                               <td>
                                     <div class="categories-table-img">
                                        <picture>
                                           <img src=${j.getFotoLogo ()} alt="category">
                                        </picture>
                                     </div>
                               </td>
                               <td>
                                ${j.getNombreJuego() }
                               </td>
                               <td>
                                  <div class="pages-table-img">
                                     ${j.getCategoria() }
                                  </div>
                               </td>
                                 <td><span class="badge-success">${ j.isPromo() ? "En Oferta" : "" }</span></td>
                               <td>$ ${j.getPrecio() }</td>
                           
                             
                                                          
                              </tr>
                          </c:forEach>
                                                            
                         </tbody>
                  </table>
               </div>
            </div>
                        <!-- ! Footer -->
     <jsp:include page="footer.jsp"/>

         </div>
         </div>
         </main>

      </div>
      </div>
      
      <!-- Chart library -->
      <script src="https://webapp.ioseg.com/img/plugins/chart.min.js"></script>
      <!-- Icons library -->
      <script src="https://webapp.ioseg.com/img/plugins/feather.min.js"></script>
      <!-- Custom scripts -->
      <script src="https://webapp.ioseg.com/js/script.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
      <script>
        jQuery(document).ready(function($) {
    $('*[data-href]').on('click', function() {
        window.location = $(this).data("href");
    });
});
      </script>
   </body>
</html>
