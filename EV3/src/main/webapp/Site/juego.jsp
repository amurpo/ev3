<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Payara Games | Juego</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
      <!-- Custom styles -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/Site/css/style.min.css" />
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
      <link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick-theme.css">
      <style>
         .col-centered{
         float: none;
         margin: 0 auto;
         
         }
      </style>
   </head>
   <body>
      <div class="layer"></div>
      <!-- ! Body -->
      <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
      <div class="page-flex">
      
           <!-- ! Sidebar -->
   <jsp:include page="menu.jsp"/>
   
      <div class="main-wrapper">
      
                 <!-- ! Main nav -->
   <jsp:include page="header.jsp"/>
   
         <br><br>
         <!-- ! Main -->
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-10 col-centered">
                  <div class="row">
                     <div class="col-md-3">
                        <article class="white-block">
                           <div class="top-cat-title">
                              <b>
                                 <h3>F1® 2021</h3>
                              </b>
                              <br>
                           </div>
                           <ul class="top-cat-list">
                              <li>
                                    <div class="top-cat-list__title">
                                       Género 
                                    </div>
                                    <div class="top-cat-list__subtitle">
                                       Deportes 
                                    </div>
                              </li>
                              <br>
                              <li>
                                    <div class="top-cat-list__title">
                                       Compañía
                                    </div>
                                    <div class="top-cat-list__subtitle">
                                       Codemasters
                                    </div>
                              </li>
                              <br>
                              <li>
                                 <div class="top-cat-list__title">
                                    Multijugador
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    SI
                                 </div>
                           </li>
                           <br>
                           <li>
                              <div class="top-cat-list__title">
                                 Fecha de lanzamiento
                              </div>
                              <div class="top-cat-list__subtitle">
                                 15 JUL 2021
                              </div>
                        </li>
                           <br><br>
                              <li>
                                    <div class="top-cat-list__title">
                                       PRECIO<span>$ 999999</span>
                           
                                    </div>
                                    <br><br>
                                 <a class="badge-success" href="./Comprar.do" role="button">COMPRAR</a>
                              </li>
                           </ul>
                        </article>

                     </div>
                     <div class="col-md-9">
                        <img alt="" src="https://cdn.cloudflare.steamstatic.com/steam/apps/1134570/ss_504e0ec2205e938fe459e02c43021c28b12f0fbf.1920x1080.jpg?t=1626784622" />
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
         </main>
         <!-- ! Footer -->
     <jsp:include page="footer.jsp"/>
        
        
         </div>
      </div>
      <!-- Chart library -->
      <script src="https://webapp.ioseg.com/img/plugins/chart.min.js"></script>
      <!-- Icons library -->
      <script src="https://webapp.ioseg.com/img/plugins/feather.min.js"></script>
      <!-- Custom scripts -->
      <script src="https://webapp.ioseg.com/js/script.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
   </body>

</html>
