

<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Payara Games | Mantenedor de Juegos</title>
         <!-- Favicon -->
   <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
</head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==">
<style>
   .col-centered{
   float: none;
   margin: 0 auto;
   }
</style>
<body>
   <div class="container" style="margin-top: 20px;">
      <div class="row">
         <div class="col-6 text-center col-centered ">
            <h1>Mantenedor de Juegos</h1>
            <div class="row" style="margin-top: 40px;">
               <div class="col-md-6">
                <a href="RegistroJuego.do">
                  <div class="class-box card text-white bg-success mb-3" style="min-height: 10rem; max-width: 18rem;">
                     <div class="card-body">
                        <h5 class="card-title">Registro de Juegos</h5>
                     </div>
                  </div>
               </div>
               </a>
               <div class="col-md-6">
               <a href="Editar.do">
                <div class="class-box card text-white bg-danger mb-3" style="min-height: 10rem; max-width: 18rem;">
                    <div class="card-body">
                       <h5 class="card-title">Editar/Listar Juegos</h5>
                    </div>
                 </div>               
                 </div>
                 </a>
            </div>
         </div>
      </div>
   </div>

</body>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/brands.min.js" integrity="sha512-vefaKmSAX3XohXhN50vLfnK12TPIO+4uRpHjXVkX726CqbicEiAQGRzsMTE+EpLkBk4noUcUYu6AQ5af2vfRLA==" crossorigin="anonymous"></script>

</html>

