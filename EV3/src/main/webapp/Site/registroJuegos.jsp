

<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Payara Games | Registro de Juegos</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
      <!-- Custom styles -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/Site/css/style.min.css" />

   </head>
   <body>
      <div class="layer"></div>
      <main class="page-center">
         <article class="sign-up">
            <h1 class="sign-up__title">Registro de Juegos</h1>
            <form class="sign-up-form form" action="RegistroJuego.do" method="post">
               <label class="form-label-wrapper">
                  <p class="form-label">Nombre de Juego</p>
                  <input class="form-input" type="text" placeholder="Ingrese nombre" id="NombreJuego" name="NombreJuego" required>
               </label>
               <label class="form-label-wrapper">
                  <p class="form-label">Cantidad</p>
                  <input class="form-input" type="text" placeholder="Ingrese stock" pattern="^[1-9]\d*$" id="Stock" name="Stock" required>
               </label>
               <div >
                  <p class="form-label">Categoría</p>
                  <select class="form-control" name="Categoria" id="Categoria">
                     <option value="">Acción</option>
                     <option value="">Deportes</option>
                     <option value="">Estrategia</option>
                     <option value="">RPG</option>
                  </select>
               </div>
               <br>
               <label class="form-label-wrapper">
                  <p class="form-label">Compañía</p>
                  <input class="form-input" type="text" placeholder="Ingrese compañía" id="Company" name="Company" required>
               </label>
               <label class="form-label-wrapper">
                  <p class="form-label">Fecha de Lanzamiento</p>
                  <input class="form-input" type="date" id="FechaLan" name="FechaLan" required>
               </label>
               <label class="form-label-wrapper">
                  <p class="form-label">Precio</p>
                  <input class="form-input" type="text" placeholder="Ingrese precio" pattern="^[1-9]\d*$" id="Precio" name="Precio"required>
               </label>
               <label class="form-label-wrapper">
                  <p class="form-label">Link foto principal</p>
                  <input class="form-input" type="text" placeholder="Ingrese ingrese link" id="LinkMain" name="LinkMain" required>
               </label>
               <label class="form-label-wrapper">
                  <p class="form-label">Link logo de juego</p>
                  <input class="form-input" type="text" placeholder="Ingrese link logo" id="LinkLogo" name="LinkLogo" required>
               </label>
               <p class="form-label">Multijugador</p>
               <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                  <label class="form-checkbox-label" for="inlineRadio1">Si</label>
               </div>
               <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                  <label class="form-checkbox-label" for="inlineRadio2">No</label>
               </div>
               <hr>
               <br>
               <label class="form-checkbox-wrapper">
               <input class="form-checkbox" type="checkbox" required>
               <span class="form-checkbox-label">Confirmar</span>
               </label>
               <button class="form-btn primary-default-btn transparent-btn">Registrar</button>
            </form>
         </article>
      </main>
      <!-- Chart library -->
      <script src="https://webapp.ioseg.com/img/plugins/chart.min.js"></script>
      <!-- Icons library -->
      <script src="https://webapp.ioseg.com/img/plugins/feather.min.js"></script>
      <!-- Custom scripts -->
      <script src="https://webapp.ioseg.com/js/script.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

   </body>
</html>

