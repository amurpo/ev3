<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Payara Games | Login</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
      <!-- Custom styles -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/Site/css/style.min.css" />
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
      <link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick-theme.css">
</head>

<body>
  <div class="layer"></div>
<main class="page-center">
  <article class="sign-up">
    <h1 class="sign-up__title">Bienvenido de nuevo!</h1>
    <p class="sign-up__subtitle">Ingrese sus datos para continuar</p>
    <form class="sign-up-form form" action="" method="post">
      <label class="form-label-wrapper">
        <p class="form-label">Email</p>
        <input class="form-input" type="email" id="mail" name="mail" placeholder="Correo" required>
      </label>
      <label class="form-label-wrapper">
        <p class="form-label">Password</p>
        <input class="form-input" type="password" id="pass" name="pass" placeholder="Contraseña" pattern=".{6,}" required>
      </label>
      <label class="form-checkbox-wrapper">
        <input class="form-checkbox" type="checkbox" required>
        <span class="form-checkbox-label">Recordar</span>
      </label>
      <button class="form-btn primary-default-btn transparent-btn" id="ingresar">Entrar</button>
    </form>
  </article>
</main>
      <!-- Chart library -->
      <script src="https://webapp.ioseg.com/img/plugins/chart.min.js"></script>
      <!-- Icons library -->
      <script src="https://webapp.ioseg.com/img/plugins/feather.min.js"></script>
      <!-- Custom scripts -->
      <script src="https://webapp.ioseg.com/js/script.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script>
      var btn = document.getElementById("ingresar");
      var mail = document.getElementById('mail');
      var pass = document.getElementById("pass");

      btn.addEventListener('click',function (evt){
        if (mail.value === "admin@mail.com"){
          location.href = "/EV3";
          return false;
        }
      });
    
</script>

</body>

</html>