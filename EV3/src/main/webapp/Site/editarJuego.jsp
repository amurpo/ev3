<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Payara Games | Edición y Lista de Juegos</title>

      <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
      <!-- Custom styles -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/Site/css/style.min.css" />
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
      <style>
         .col-centered{
         float: none;
         margin: 0 auto;
         }
      </style>
   </head>
   <body>
      <!-- ! Body -->
      <div >
         <div>
         <br>
            <!-- ! Main -->
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12 col-centered">
                   
                              <div class="col-md-12">
                                 <h1 style="text-align: center;">Lista de Juegos</h1>
                              </div>
                              <div class="col-md-12">
                                 <table class="table table-bordered table-striped" id="tabla-trabajo">
                                    <thead>
                                       <tr>
                                          <th>Nombre de juego</th>
                                          <th>Stock</th>
                                          <th>Categoría</th>
                                          <th>Compañía</th>
                                          <th>Fecha</th>
                                          <th>Precio</th>
                                          <th>Link foto</th>
                                          <th>Link logo</th>
                                          <th>Multi</th>
                                          <th>Promo</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                    
                                       <c:forEach items="${listJuegos}"  var="j" varStatus="recorrido">
                                          <tr>
                                             <td>${j.getNombreJuego() }</td>
                                             <td>${j.getStock() }</td>
                                             <td>${j.getCategoria ()}</td>
                                             <td>${j.getCompany ()} </td>
                                             <td>${j.getFechaLan ()}</td>
                                             <td>${j.getPrecio ()}</td>
                                             <td>${j.getFotoMain ()} </td>
                                             <td>${j.getFotoLogo ()}</td>
                                             <td>${ j.isMultiplayer() ? "Si" : "No" }</td>
                                             <td>${ j.isPromo() ? "Si" : "No" }</td>
                                             <td class="text-center"><a class="btn btn-circle btn-success" href="Editar.do?${j.getIdJuego()}">Editar</a></td>
                            				 <td class="text-center"><button class="btn btn-sm btn-danger" onclick="deleteJuego(${j.getIdJuego()},'Nombre de juego fila')">Eliminar</button></td>
                                          </tr>
                                       </c:forEach>
                                    </tbody>
                                 </table>
                              </div>

                     </div>
                  </div>
               </div>
            </div>
            </main>
         </div>
      </div>

      <!-- Custom scripts -->
      <script src="https://webapp.ioseg.com/js/script.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
   </body>
</html>

