<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Payara Games | Inicio</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
      <!-- Custom styles -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/Site/css/style.min.css" />
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
      <link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick-theme.css">
   </head>
   <body>
      <div class="layer"></div>
      <!-- ! Body -->
      <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
      <div class="page-flex">
      <!-- ! Sidebar -->
   <jsp:include page="menu.jsp"/>
     
     
      <div class="main-wrapper">
         <!-- ! Main nav -->
   <jsp:include page="header.jsp"/>



         <!-- ! Main -->
         <main class="main users chart-page" id="skip-target">
            <div class="container">
               <h2 class="main-title">Estadísticas</h2>
               <div class="row stat-cards">
                  <div class="col-md-6 col-xl-3">
                     <article class="stat-cards-item">
                        <div class="stat-cards-icon primary">
                           <i data-feather="bar-chart-2" aria-hidden="true"></i>
                        </div>
                        <div class="stat-cards-info">
                           <p class="stat-cards-info__num">1478 286</p>
                           <p class="stat-cards-info__title">Usuarios registrados totales</p>
                           <p class="stat-cards-info__progress">
                              <span class="stat-cards-info__profit success">
                              <i data-feather="trending-up" aria-hidden="true"></i>4.07%
                              </span>
                              Último mes
                           </p>
                        </div>
                     </article>
                  </div>
                  <div class="col-md-6 col-xl-3">
                     <article class="stat-cards-item">
                        <div class="stat-cards-icon warning">
                           <i data-feather="file" aria-hidden="true"></i>
                        </div>
                        <div class="stat-cards-info">
                           <p class="stat-cards-info__num">1478 286</p>
                           <p class="stat-cards-info__title">Ventas totales</p>
                           <p class="stat-cards-info__progress">
                              <span class="stat-cards-info__profit success">
                              <i data-feather="trending-up" aria-hidden="true"></i>0.24%
                              </span>
                              Último mes
                           </p>
                        </div>
                     </article>
                  </div>
                  <div class="col-md-6 col-xl-3">
                     <article class="stat-cards-item">
                        <div class="stat-cards-icon purple">
                           <i data-feather="file" aria-hidden="true"></i>
                        </div>
                        <div class="stat-cards-info">
                           <p class="stat-cards-info__num">1478 286</p>
                           <p class="stat-cards-info__title">Usuarios registrados al día</p>
                           <p class="stat-cards-info__progress">
                              <span class="stat-cards-info__profit danger">
                              <i data-feather="trending-down" aria-hidden="true"></i>1.64%
                              </span>
                              Ayer
                           </p>
                        </div>
                     </article>
                  </div>
                  <div class="col-md-6 col-xl-3">
                     <article class="stat-cards-item">
                        <div class="stat-cards-icon success">
                           <i data-feather="feather" aria-hidden="true"></i>
                        </div>
                        <div class="stat-cards-info">
                           <p class="stat-cards-info__num">1478 286</p>
                           <p class="stat-cards-info__title">Ventas diarias</p>
                           <p class="stat-cards-info__progress">
                              <span class="stat-cards-info__profit warning">
                              <i data-feather="trending-up" aria-hidden="true"></i>0.00%
                              </span>
                              Ayer
                           </p>
                        </div>
                     </article>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-9 offset-lg-1">
                     <div class="slide" style="margin:20px">
                        <div>
                           <a href="#">
                              <img class="img-responsive" src="https://cdn.cloudflare.steamstatic.com/steam/apps/1134570/ss_504e0ec2205e938fe459e02c43021c28b12f0fbf.1920x1080.jpg?t=1626784622" alt="">
                              <div class="overlay">
                                 <div class="overlay-content">
                                    <h3>F1® 2021</h3>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div>
                           <a href="#">
                              <img class="img-responsive" src="https://cdn.cloudflare.steamstatic.com/steam/apps/952060/ss_77eda710487b89293f109cf7dcf96b4ffab0d1a1.1920x1080.jpg?t=1614737794" alt="">
                              <div class="overlay">
                                 <div class="overlay-content">
                                    <h3>Resident Evil 3</h3>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <div>
                           <a href="#">
                              <img class="img-responsive" src="https://cdn.cloudflare.steamstatic.com/steam/apps/1190460/ss_a844f976c086d72f91de4a30a38c80e781988653.1920x1080.jpg?t=1616683107" alt="">
                              <div class="overlay">
                                 <div class="overlay-content">
                                    <h3>DEATH STRANDING</h3>
                                 </div>
                              </div>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div style="margin-top: 20px;" class="col-lg-3">
                     <article class="white-block">
                        <div class="top-cat-title">
                           <b>
                              <h3>Juegos más vendidos</h3>
                           </b>
                           <p>Últimos 3 meses</p>
                        </div>
                        <ul class="top-cat-list">
                           <li>
                              <a href="./Juego.do">
                                 <div class="top-cat-list__title">
                                    F1® 2021 <span>8.2k</span>
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    Deportes <span class="purple">+472</span>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="./Juego.do">
                                 <div class="top-cat-list__title">
                                    Resident Evil 3 <span>8.2k</span>
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    Acción <span class="blue">+472</span>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="./Juego.do">
                                 <div class="top-cat-list__title">
                                    Sid Meier’s Civilization® VI <span>8.2k</span>
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    Estrategia <span class="danger">+472</span>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="./Juego.do">
                                 <div class="top-cat-list__title">
                                    DEATH STRANDING <span>8.2k</span>
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    Acción <span class="success">+472</span>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="./Juego.do">
                                 <div class="top-cat-list__title">
                                    FINAL FANTASY XIV Online <span>8.2k</span>
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    RPG <span class="warning">+472</span>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="./Juego.do">
                                 <div class="top-cat-list__title">
                                    EA SPORTS™ FIFA 21 <span>8.2k</span>
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    Deportes <span class="warning">+472</span>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="./Juego.do">
                                 <div class="top-cat-list__title">
                                    Monster Hunter Stories 2: Wings of Ruin <span>8.2k</span>
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    RPG <span class="warning">+472</span>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="./Juego.do">
                                 <div class="top-cat-list__title">
                                    Halo: The Master Chief Collection <span>8.2k</span>
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    Acción <span class="warning">+472</span>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="./Juego.do">
                                 <div class="top-cat-list__title">
                                    Super Robot Wars 30 <span>8.2k</span>
                                 </div>
                                 <div class="top-cat-list__subtitle">
                                    Estrategia <span class="warning">+472</span>
                                 </div>
                              </a>
                           </li>
                        </ul>
                     </article>
                  </div>
               </div>
         </main>
         <!-- ! Footer -->
     <jsp:include page="footer.jsp"/>
        
        
        
         </div>
      </div>
      <!-- Chart library -->
      <script src="https://webapp.ioseg.com/img/plugins/chart.min.js"></script>
      <!-- Icons library -->
      <script src="https://webapp.ioseg.com/img/plugins/feather.min.js"></script>
      <!-- Custom scripts -->
      <script src="https://webapp.ioseg.com/js/script.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
   </body>
   <script>
      $(document).ready(function(){
         $('.slide').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      });
      })
   </script>
</html>
    