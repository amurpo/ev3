<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  


<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Payara Games | Juegos RPG</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="https://webapp.ioseg.com/img/avatar/índice.png" type="image/x-icon">
      <!-- Custom styles -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/Site/css/style.min.css" />
      <style>
         .col-centered{
         float: none;
         margin: 0 auto;
         
         }
         [data-href] {
    cursor: pointer;
}
      </style>
   </head>
   <body>
      <div class="layer"></div>
      <!-- ! Body -->
      <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
      <div class="page-flex">
      
             <!-- ! Sidebar -->
   <jsp:include page="menu.jsp"/>
     
     
      <div class="main-wrapper">
         <!-- ! Main nav -->
   <jsp:include page="header.jsp"/>
   
            <!-- ! Main -->
            <main class="main users chart-page" id="skip-target">
               <div class="container">
               </div>
               <br>
               <div class="row">
                  <div class="col-lg-9 col-centered">
                     <h2 style="text-align: center;" class="main-title">Juegos RPG</h2>
						<br>
                     <div class="users-table table-wrapper">
                        <table class="posts-table">
                           <thead>

                           </thead>
                           <tbody>
                           
                           <c:forEach items="${listJuegosRPG}"  var="j" varStatus="recorrido">
                             
                              <tr data-href="Juego.do">
                                 <td>
                                       <div class="categories-table-img">
                                          <picture>
                                             <img src=${j.getFotoLogo ()} alt="category">
                                          </picture>
                                       </div>
                                 </td>
                                 <td>
                                  ${j.getNombreJuego() }
                                 </td>
                                 <td>
                                    <div class="pages-table-img">
                                       ${j.getCategoria() }
                                    </div>
                                 </td>
                                 <td><span class="badge-success">${ j.isPromo() ? "En Oferta" : "" }</span></td>
                                 <td>$ ${j.getPrecio() }</td>
                             
                              </tr>
                           
                           </c:forEach>
                                                            
                         </tbody>
                  </table>
               </div>
            </div>
                        <!-- ! Footer -->
     <jsp:include page="footer.jsp"/>

         </div>
         </div>
         </main>

      </div>
      </div>
      
      <!-- Chart library -->
      <script src="https://webapp.ioseg.com/img/plugins/chart.min.js"></script>
      <!-- Icons library -->
      <script src="https://webapp.ioseg.com/img/plugins/feather.min.js"></script>
      <!-- Custom scripts -->
      <script src="https://webapp.ioseg.com/js/script.js"></script>
      <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
      <script>
        jQuery(document).ready(function($) {
    $('*[data-href]').on('click', function() {
        window.location = $(this).data("href");
    });
});
      </script>
   </body>
</html>
