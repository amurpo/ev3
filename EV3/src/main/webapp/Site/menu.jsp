<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
 <aside class="sidebar">
         <div class="sidebar-start">
            <div class="sidebar-head">
               <span class="sr-only">Home</span>
               <span class="icon logo" aria-hidden="true"></span>
               <div class="logo-text">
                  <span class="logo-title">Payara</span>
                  <span class="logo-subtitle">Games</span>
               </div>
               <button class="sidebar-toggle transparent-btn" title="Menu" type="button">
               <span class="sr-only">Toggle menu</span>
               <span class="icon menu-toggle" aria-hidden="true"></span>
               </button>
            </div>
            <div class="sidebar-body">
               <ul class="sidebar-body-menu">
                  <li>
                     <a href="/EV3" ><span class="icon home" title="Home" aria-hidden="true"></span>Home</a>
                  </li>
                  <li>
                     <a href="./Ofertas.do"><span class="icon star" aria-hidden="true"></span>Ofertas
                     </a>
                  </li>
                  <li>
                     <a class="show-cat-btn" href="##">
                     <span class="icon folder" aria-hidden="true"></span>Categorías
                     <span class="category__btn transparent-btn" title="Listar">
                     <span class="sr-only">Lista</span>
                     <span class="icon arrow-down" aria-hidden="true"></span>
                     </span>
                     </a>
                     <ul class="cat-sub-menu">
                        <li>
                           <a href="Categorias.do">Todas las categorías</a>
                        </li>
                        <li>
                           <a href="Accion.do">Acción</a>
                        </li>
                        <li>
                           <a href="Deporte.do">Deportes</a>
                        </li>
                        <li>
                           <a href="Estrategia.do">Estrategia</a>
                        </li>
                        <li>
                           <a href="RPG.do">RPG</a>
                        </li>
                     </ul>
                  </li>
                  <li>
                     <a href="./Nosotros.do" ><span class="icon user-3" aria-hidden="true"></span>Sobre nosotros</a>
                  </li>
               </ul>
            </div>
         </div>
      </aside>