<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
         <nav class="main-nav--bg">
            <div class="container main-nav">
               <div class="main-nav-start">
               </div>
               <div class="main-nav-end">
                  <button class="sidebar-toggle transparent-btn" title="Menu" type="button">
                  <span class="sr-only">Toggle menu</span>
                  <span class="icon menu-toggle--gray" aria-hidden="true"></span>
                  </button>
                  <button class="theme-switcher gray-circle-btn" type="button" title="Cambiar tema">
                  <span class="sr-only">Cambiar tema</span>
                  <i class="sun-icon" data-feather="sun" aria-hidden="true"></i>
                  <i class="moon-icon" data-feather="moon" aria-hidden="true"></i>
                  </button>
                  <div class="nav-user-wrapper">
                     <button href="##" class="nav-user-btn dropdown-btn" title="Mi perfil" type="button">
                        <span class="sr-only">Mi perfil</span>
                        <span class="nav-user-img">
                           <picture>
                              <source srcset="https://webapp.ioseg.com/img/avatar/avatar-illustrated-02.webp" type="image/webp">
                              <img src="https://webapp.ioseg.com/img/avatar/avatar-illustrated-02.png" alt="User name">
                           </picture>
                        </span>
                     </button>
                     <ul class="users-item-dropdown nav-user-dropdown dropdown">
                        <li><a href="./Ingresar.do">
                           <i data-feather="user" aria-hidden="true"></i>
                           <span>Ingresar</span>
                           </a>
                        </li>
                        <li><a href="./Registrar.do">
                           <i data-feather="settings" aria-hidden="true"></i>
                           <span>Registrar</span>
                           </a>
                        </li>
                        <li><a class="danger" href="##">
                           <i data-feather="log-out" aria-hidden="true"></i>
                           <span>Salir</span>
                           </a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </nav>