package Model;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="juegos")

@NamedQueries({
	@NamedQuery(name="Juego.getAllJuegos",query="select j from Juego j")
})

public class Juego implements Serializable{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idJuegos;
	
	@Column(name="nombreJuego")
	private String nombreJuego;
	
	@Column(name="stock")
	private int stock;
	
	@Column(name="categoria")
	private String categoria;
	
	@Column(name="company")
	private String company;
	
	@Column(name="fechaLan")
	private Date fechaLan;
	
	@Column(name="precio")
	private int precio;
	
	@Column(name="fotoMain")
	private String fotoMain;
	
	@Column(name="fotoLogo")
	private String fotoLogo;
	
	@Column(name="multiplayer")
	private boolean multiplayer;
	
	@Column(name="promo")
	private boolean promo;
	
	public int getIdJuego() {
		return idJuegos;
	}
	public void setIdJuego(int idJuego) {
		this.idJuegos = idJuego;
	}
	
	public String getNombreJuego() {
		return nombreJuego;
	}
	public void setNombreJuego(String nombreJuego) {
		this.nombreJuego = nombreJuego;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Date getFechaLan() {
		return fechaLan;
	}
	public void setFechaLan(Date fechaLan) {
		this.fechaLan = fechaLan;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public String getFotoMain() {
		return fotoMain;
	}
	public void setFotoMain(String fotoMain) {
		this.fotoMain = fotoMain;
	}
	public String getFotoLogo() {
		return fotoLogo;
	}
	public void setFotoLogo(String fotoLogo) {
		this.fotoLogo = fotoLogo;
	}
	public boolean isMultiplayer() {
		return multiplayer;
	}
	public void setMultiplayer(boolean multiplayer) {
		this.multiplayer = multiplayer;
	}
	public boolean isPromo() {
		return promo;
	}
	public void setPromo(boolean promo) {
		this.promo = promo;
	}
	
	@Override
	public String toString() {
		return "Juego [idJuegos=" + idJuegos + ", nombreJuego=" + nombreJuego + ", stock=" + stock + ", categoria="
				+ categoria + ", company=" + company + ", fechaLan=" + fechaLan + ", precio=" + precio + ", fotoMain="
				+ fotoMain + ", fotoLogo=" + fotoLogo + ", multiplayer=" + multiplayer + ", promo=" + promo + "]";
	}
	
	
}
