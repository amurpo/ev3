package Servicios;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import Model.Juego;

/**
 * Session Bean implementation class JuegosService
 */
@Stateless
@LocalBean
public class JuegosService implements JuegosServiceLocal {

    /**
     * Default constructor. 
     */
    public JuegosService() {
        // TODO Auto-generated constructor stub
    }
    
    private EntityManagerFactory emf= Persistence.createEntityManagerFactory("EV3EJB");

	@Override
	public void addJuego(Juego j) {
		EntityManager em=this.emf.createEntityManager();
		try {
			em.persist(j);
			em.flush();
		}catch (Exception e) {
			
		}finally {
			em.close();
		}
	}

	@Override
	public List<Juego> getAllJuegos() {
		EntityManager em=this.emf.createEntityManager();
		try {
			return em.createNamedQuery("Juego.getAllJuegos",Juego.class).getResultList();
		}catch (Exception e) {
			
		}finally {
			em.close();
		}
		return null;
	}

	@Override
	public void removeJuego(Juego j) {
		EntityManager em=this.emf.createEntityManager();
		try {
			em.remove(em.find(Juego.class, j.getIdJuego()));
					
		}catch (Exception e) {
			
		}finally {
			em.close();
		}		
	}

	@Override
	public void updateJuego(Juego j) {
		EntityManager em=this.emf.createEntityManager();
		try {
			Juego origen=em.find(Juego.class, j.getIdJuego());
			origen.setNombreJuego(j.getNombreJuego());
			origen.setStock(j.getStock());
			origen.setCategoria(j.getCategoria());
			origen.setCompany(j.getCompany());
			origen.setFechaLan(j.getFechaLan());
			origen.setPrecio(j.getPrecio());
			origen.setFotoMain(j.getFotoMain());
			origen.setFotoLogo(j.getFotoLogo());
			origen.setMultiplayer(j.isMultiplayer());
			origen.setPromo(j.isPromo());

			
			em.merge(origen);
			em.flush();
			
		}catch (Exception e) {
			
		}finally {
			em.close();
		}	 		
	}

}
