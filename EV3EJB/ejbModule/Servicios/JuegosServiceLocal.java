package Servicios;

import java.util.List;

import javax.ejb.Local;

import Model.Juego;

@Local
public interface JuegosServiceLocal {
	
	public void addJuego(Juego j);
	public List<Juego> getAllJuegos();
	public void removeJuego(Juego j);
	public void updateJuego(Juego j);

}
