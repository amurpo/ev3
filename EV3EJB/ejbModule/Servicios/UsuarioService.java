package Servicios;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import Model.Usuario;

/**
 * Session Bean implementation class UsuarioService
 */
@Stateless
@LocalBean
public class UsuarioService implements UsuarioServiceLocal {

    /**
     * Default constructor. 
     */
    public UsuarioService() {
        // TODO Auto-generated constructor stub
    }
    
    private EntityManagerFactory emf= Persistence.createEntityManagerFactory("EV3EJB");

	@Override
	public void addUsuario(Usuario u) {
		EntityManager em=this.emf.createEntityManager();
		try {
			em.persist(u);
			em.flush();
		}catch (Exception e) {
			
		}finally {
			em.close();
		}
				
	}

	@Override
	public List<Usuario> getAllUsuarios() {
		EntityManager em=this.emf.createEntityManager();
		try {
			return em.createNamedQuery("usuario.getAllUsuarios",Usuario.class).getResultList();
		}catch (Exception e) {
			
		}finally {
			em.close();
		}
		return null;
	}

	@Override
	public void removeUsuario(Usuario u) {
		EntityManager em=this.emf.createEntityManager();
		try {
			em.remove(em.find(Usuario.class, u.getIdUser()));
					
		}catch (Exception e) {
			
		}finally {
			em.close();
		}			
	}

	@Override
	public void updateUsuario(Usuario u) {
		EntityManager em=this.emf.createEntityManager();
		try {
			Usuario origen=em.find(Usuario.class, u.getIdUser());
			origen.setNombre(u.getNombre());
			origen.setPassword(u.getPassword());
			origen.setRut(u.getRut());
			origen.setEmail(u.getEmail());
			
			em.merge(origen);
			em.flush();
			
		}catch (Exception e) {
			
		}finally {
			em.close();
		}		
	}

}
