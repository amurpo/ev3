package Servicios;

import java.util.List;

import javax.ejb.Local;

import Model.Usuario;

@Local
public interface UsuarioServiceLocal {
	public void addUsuario(Usuario u);
	public List<Usuario> getAllUsuarios();
	public void removeUsuario(Usuario u);
	public void updateUsuario(Usuario u);
}
